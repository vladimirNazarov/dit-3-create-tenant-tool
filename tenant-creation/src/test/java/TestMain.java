import api.client.ApiException;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static api.helpers.ApiHelper.addTenantByApi;
import static api.helpers.AppConfiguration.TENANT_NAME;
import static utils.DbHelper.cleanUpBeforeAddTenant;
import static utils.OSHelper.replaceSyncTestUrl;
import static utils.OSHelper.restartEngine;


public class TestMain {
    @Test
    public void test() throws SQLException, ApiException {
        replaceSyncTestUrl();
        cleanUpBeforeAddTenant(TENANT_NAME);
        addTenantByApi(TENANT_NAME);
        restartEngine();
    }
}
