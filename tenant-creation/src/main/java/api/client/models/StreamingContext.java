/*
 * DictateIT 3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package api.client.models;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModelProperty;

import java.io.IOException;
import java.util.Objects;

/**
 * StreamingContext
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-04-02T11:43:45.649+03:00")
public class StreamingContext {
    @SerializedName("context")
    private Object context = null;
    @SerializedName("state")
    private StateEnum state = null;

    public StreamingContext context(Object context) {
        this.context = context;
        return this;
    }

    /**
     * Get context
     *
     * @return context
     **/
    @ApiModelProperty(value = "")
    public Object getContext() {
        return context;
    }

    public void setContext(Object context) {
        this.context = context;
    }

    /**
     * Get state
     *
     * @return state
     **/
    @ApiModelProperty(value = "")
    public StateEnum getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StreamingContext streamingContext = (StreamingContext) o;
        return Objects.equals(this.context, streamingContext.context) &&
                Objects.equals(this.state, streamingContext.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(context, state);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class StreamingContext {\n");

        sb.append("    context: ").append(toIndentedString(context)).append("\n");
        sb.append("    state: ").append(toIndentedString(state)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    /**
     * Gets or Sets state
     */
    @JsonAdapter(StateEnum.Adapter.class)
    public enum StateEnum {
        NUMBER_1(1),

        NUMBER_2(2),

        NUMBER_4(4),

        NUMBER_8(8),

        NUMBER_16(16),

        NUMBER_32(32),

        NUMBER_64(64),

        NUMBER_128(128),

        NUMBER_255(255);

        private Integer value;

        StateEnum(Integer value) {
            this.value = value;
        }

        public static StateEnum fromValue(String text) {
            for (StateEnum b : StateEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }

        public Integer getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        public static class Adapter extends TypeAdapter<StateEnum> {
            @Override
            public void write(final JsonWriter jsonWriter, final StateEnum enumeration) throws IOException {
                jsonWriter.value(enumeration.getValue());
            }

            @Override
            public StateEnum read(final JsonReader jsonReader) throws IOException {
                Integer value = jsonReader.nextInt();
                return StateEnum.fromValue(String.valueOf(value));
            }
        }
    }

}

