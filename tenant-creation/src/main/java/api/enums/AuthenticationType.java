package api.enums;


public enum AuthenticationType {
    REGULAR(1, "Regular"),
    ACTIVE_DIRECTORY(2, "ActiveDirectory"),
    ACTIVE_DIRECTORY_SSL(3, "ActiveDirectoryWithSSL");

    Integer code;
    String name;

    AuthenticationType(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
