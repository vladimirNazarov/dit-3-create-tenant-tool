package api.helpers;


import api.client.ApiException;
import api.extra.models.TenantModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static api.enums.AuthenticationType.REGULAR;
import static api.helpers.ApiConfiguration.API_CLIENT;
import static api.helpers.ApiConfiguration.TENANT_API;
import static api.helpers.AppConfiguration.*;
import static api.responses.TenantsResponses.getAllTenants;
import static io.restassured.RestAssured.given;

public class ApiHelper {

    private ApiHelper() {
    }

    public static String getAccessToken(String login, String password, String scopes) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("grant_type", scopes);
        requestParams.put("username", login);
        requestParams.put("password", password);

        String accessToken = given()
                .header("Authorization", "Basic Unoya2R1Y3U6c1NjV2JWa2hTcDVxMW1SUkdVdlBhcWVqbzZDQWtxajY=")
                .contentType("application/x-www-form-urlencoded")
                .params(requestParams)
                .post(PORTAL_URL + "/api/OAuth/Token")
                .then().extract().path("access_token");
        return accessToken;
    }

    private static Integer getNewTenantId(List<TenantModel> tenants) {
        Integer id = 0;
        for (TenantModel tenant :
                tenants) {
            if (id < tenant.getId()) return id;

            id++;
        }
        return id;
    }

    public static void addTenantByApi(String tenantName) throws ApiException {
        API_CLIENT.setAccessToken(getAccessToken(PLATFORM_ADMINISTRATOR_LOGIN, PLATFORM_ADMINISTRATOR_PASSWORD, "password"));
        api.client.models.TenantModel tenantModel = new api.client.models.TenantModel();
        tenantModel.setId(getNewTenantId(getAllTenants()));
        tenantModel.setName(tenantName);
        tenantModel.setIsDeleted(false);
        tenantModel.setAuthenticationType(api.client.models.TenantModel.AuthenticationTypeEnum
                .fromValue(REGULAR.getCode().toString()));

        TENANT_API.tenantCreate(tenantModel);
        System.out.println("Tenant \"" + tenantName + "\" has been created by Api request");
    }

    public static String getNewUuid() {
        return UUID.randomUUID().toString();
    }
}
