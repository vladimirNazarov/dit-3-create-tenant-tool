package api.helpers;

import utils.PropertyProvider;

public interface AppConfiguration {
    PropertyProvider CONFIG = new PropertyProvider();
    String TENANT_NAME = CONFIG.getStringProperty("tenant.name");
    String PLATFORM_ADMINISTRATOR_LOGIN = CONFIG.getStringProperty("users.platform.admin.login");
    String PLATFORM_ADMINISTRATOR_PASSWORD = CONFIG.getStringProperty("users.platform.admin.password");
    String PORTAL_URL = CONFIG.getStringProperty("portal.url");
}
