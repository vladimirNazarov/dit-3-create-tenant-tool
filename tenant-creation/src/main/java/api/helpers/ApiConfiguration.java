package api.helpers;

import api.client.ApiClient;
import api.client.api.TenantApi;
import api.extra.api.ExtraTenantApi;

import static api.helpers.ApiHelper.getNewUuid;

public class ApiConfiguration {

    public static final String CLIENT_ID = getNewUuid();
    public static ApiClient API_CLIENT;

    static {
        API_CLIENT = new ApiClient()
                .setReadTimeout(120000)
                .setWriteTimeout(120000)
                .setConnectTimeout(120000)
                .setBasePath(AppConfiguration.PORTAL_URL + "/api")
                .addDefaultHeader("ClientId", CLIENT_ID);
    }

    public static final TenantApi TENANT_API = new TenantApi(API_CLIENT);
    public static final ExtraTenantApi EXTRA_TENANT_API = new ExtraTenantApi(API_CLIENT);
}
