package api.responses;


import api.client.ApiException;
import api.client.ApiResponse;
import api.extra.models.JsonResultIEnumerableTenantModel;
import api.extra.models.TenantModel;

import java.util.List;

import static api.helpers.ApiConfiguration.EXTRA_TENANT_API;


public class TenantsResponses {
    private TenantsResponses() {
    }

    public static ApiResponse<JsonResultIEnumerableTenantModel> getAllTenantsWithHttpInfo() throws ApiException {
        return EXTRA_TENANT_API.tenantsGetAllWithHttpInfo();
    }

    public static List<TenantModel> getAllTenants() throws ApiException {
        return EXTRA_TENANT_API.tenantsGetAll().getValue();
    }
}
