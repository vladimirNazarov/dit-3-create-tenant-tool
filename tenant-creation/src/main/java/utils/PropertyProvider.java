package utils;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Loads test suite configuration from resource files.
 */
public class PropertyProvider {

    private static Properties properties;

    public PropertyProvider() {
        this(System.getProperty("config.properties"));

    }

    private PropertyProvider(String fromResource) {
        properties = new Properties();
        try (InputStream inputStream = PropertyProvider.class.getResourceAsStream(fromResource)) {
            properties.load(inputStream);
        } catch (IOException e) {
            System.out.println("Can't read properties file");
            e.printStackTrace();
        }
    }

    public boolean hasProperty(String name) {
        return properties.containsKey(name);
    }

    public String getProperty(String name) {
        return properties.getProperty(name);
    }

    public String getStringProperty(String property) {
        return properties.getProperty(property);
    }

    public int getIntProperty(String property) {
        System.out.println(property);
        return Integer.parseInt(properties.getProperty(property));
    }

    public long getLongProperty(String property) {
        return Long.parseLong(properties.getProperty(property));
    }
}
