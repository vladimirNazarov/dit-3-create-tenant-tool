package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static api.helpers.AppConfiguration.CONFIG;


public class OSHelper {
    private static final String cmdString = "cmd.exe";
    private static final String consoleOptionString = "/c";
    private static final String wmicCommandHeader = "wmic " +
            "/user:'" + CONFIG.getStringProperty("dvr.user.name") + "' " +
            "/password:'" + CONFIG.getStringProperty("dvr.password") + "' " +
            "/node:'" + CONFIG.getStringProperty("dvr.ip") + "' ";

    public static void restartEngine() {
        ProcessBuilder restartEngineBuilder = new ProcessBuilder(
                cmdString, consoleOptionString, wmicCommandHeader +
                "process call create \"PowerShell restart-service 'DictateIT Engine'\"");
        try {
            restartEngineBuilder.start();
        } catch (IOException e) {
            System.out.println(e.toString());
            System.out.println("Could not restart Dictate Engine. Check it manually");
        }

        ProcessBuilder checkEngineRunningBuilder = new ProcessBuilder(
                cmdString, consoleOptionString, wmicCommandHeader +
                "service where (Caption = 'DictateIT Engine') get State");
        checkEngineRunningBuilder.redirectErrorStream(true);
        try {
            Process checkEngineRunningProcess = checkEngineRunningBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(checkEngineRunningProcess.getInputStream()));
            String line;
            String status = "Unknown";
            while (true) {
                line = reader.readLine();
                if (reader.readLine() == null) {
                    break;
                }
                if (!line.equals("")) {
                    status = line;
                }
            }
            System.out.println("Dictate Engine has been restarted and its status is " + status);
        } catch (IOException e) {
            System.out.println(e.toString());
            System.out.println("Could not check if Dictate Engine is running. Check it manually");
        }
    }

    public static void replaceSyncTestUrl() {
        ProcessBuilder replaceSyncTestUrlBuilder = new ProcessBuilder(
                cmdString, consoleOptionString, wmicCommandHeader +
                "process call create \"C:\\Users\\vladimir.nazarov\\Desktop\\desktop\\replace_synctest_url.bat\"");
        try {
            replaceSyncTestUrlBuilder.start();
            System.out.println("SyncTest URL has been replaced");
        } catch (IOException e) {
            System.out.println(e.toString());
            System.out.println("Could not replace SyncTest URL");
        }
    }
}
