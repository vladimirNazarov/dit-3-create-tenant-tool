package utils;

import java.sql.*;

import static api.helpers.AppConfiguration.CONFIG;

public final class DbHelper {

    private final static String DB_USER_NAME = CONFIG.getStringProperty("db.user.name");
    private final static String DB_PASSWORD = CONFIG.getStringProperty("db.user.password");
    private final static String DB_URL = "jdbc:sqlserver://" + CONFIG.getStringProperty("db.host");

    private DbHelper() {
    }


    private static Connection prepareConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL, DB_USER_NAME, DB_PASSWORD);
    }

    public static String getLoginSessionId(String tenantName) throws SQLException {
        String sessionId;
        String sql = "SELECT TOP(1) session_id FROM sys.dm_exec_sessions WHERE login_name = 'DIT3-Tenant-" + tenantName + "'";
        try (Connection connection = prepareConnection(); Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            if (resultSet.next()) {
                sessionId = resultSet.getString("session_id");
            } else {
                return null;
            }
        }
        return sessionId;
    }

    public static void killLoginSessionId(String tenantName) throws SQLException {
        String sessionId = getLoginSessionId(tenantName);
        while (sessionId != null) {
            String sql = "KILL %s";
            try (Connection connection = prepareConnection(); Statement statement = connection.createStatement()) {
                statement.executeUpdate(String.format(sql, sessionId));
            }
            sessionId = getLoginSessionId(tenantName);
        }
    }

    public static void cleanPlatformDatabase(String tenantName) throws SQLException {
        String sql = "USE [DIT3-Platform]\n" +
                "DELETE FROM Logger.Logs WHERE TenantId IN (SELECT id FROM Platform.Tenants WHERE Name = '" + tenantName + "')\n" +
                "DELETE FROM Platform.AllowedIPs WHERE Tenant_Id IN (SELECT id FROM Platform.Tenants WHERE Name = '" + tenantName + "')\n" +
                "DELETE FROM Platform.DeviceAuthenticationInformation WHERE Account_Id IN (SELECT id FROM Platform.Accounts WHERE Tenant_Id IN (SELECT id FROM Platform.Tenants WHERE Name = '" + tenantName + "'))\n" +
                "DELETE FROM Platform.AccountPermissions WHERE Account_Id IN (SELECT id FROM Platform.Accounts WHERE Tenant_Id IN (SELECT id FROM Platform.Tenants WHERE Name = '" + tenantName + "'))\n" +
                "DELETE FROM Platform.AccountRoles WHERE Account_Id IN (SELECT id FROM Platform.Accounts WHERE Tenant_Id IN (SELECT id FROM Platform.Tenants WHERE Name = '" + tenantName + "'))\n" +
                "DELETE FROM Platform.Accounts WHERE Tenant_Id IN (SELECT id FROM Platform.Tenants WHERE Name = '" + tenantName + "')\n" +
                "DELETE FROM Platform.Tenants WHERE Name = '" + tenantName + "'\n" +
                "DROP USER IF EXISTS [DIT3-Tenant-" + tenantName + "]";
        try (Connection connection = prepareConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    public static void dropUser(String tenantName) throws SQLException {
        String sql = "IF (EXISTS (SELECT * FROM master.sys.server_principals WHERE name = 'DIT3-Tenant-" + tenantName + "'))\n" +
                "BEGIN\n" +
                "DROP LOGIN [DIT3-Tenant-" + tenantName + "]\n" +
                "END\n" +
                "USE [master]\n" +
                "DROP USER IF EXISTS [DIT3-Tenant-" + tenantName + "]\n";
        try (Connection connection = prepareConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    public static void dropDatabase(String tenantName) throws SQLException {
        String sql = "IF (EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = 'DIT3-Data-" + tenantName + "'))\n" +
                "BEGIN\n" +
                "ALTER DATABASE [DIT3-Data-" + tenantName + "] SET single_user WITH ROLLBACK IMMEDIATE;\n" +
                "USE [master]\n" +
                "DROP DATABASE IF EXISTS [DIT3-Data-" + tenantName + "]\n" +
                "END\n";
        try (Connection connection = prepareConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    public static void cleanUpBeforeAddTenant(String tenantName) throws SQLException {
        killLoginSessionId(tenantName);
        dropDatabase(tenantName);
        dropUser(tenantName);
        cleanPlatformDatabase(tenantName);
        System.out.println("Old data for \"" + tenantName + "\" has been removed from DB");
    }
}
